//for each:
var fruits = ["apple", "orange", "cherry"];

function myFunction(item, index) {
    fruits[index] = "pineapple"
    console.log("index: " + index + " item: " + item)
}

Array.prototype.newForEach = function(callBack){

    for(let index = 0;index < this.length;index++){
        
        callBack(this[index], index, this);
    }
}

fruits.newForEach(myFunction);

//map:
Array.prototype.newMap = function(callBack){

    let newArray = []

    for(let index = 0;index < this.length;index++){

        newArray.push(callBack(this[index], index, this))
    }
    return newArray
}

let nums = [1,2,3,4,5,6,7,8,9,10]
let newNums = nums.newMap(e => e%2===0 && e)

console.log(newNums)

//some:
Array.prototype.newSome = function(callBack){

    for(let index = 0;index < this.length;index++){

        if(callBack(this[index], index, this)) return true
    }
    return false
}

console.log(nums.newSome(e => e%2===0 && e))

//find:
Array.prototype.newFind = function(callBack){

    for(let index = 0;index < this.length;index++){
        
        if(callBack(this[index], index, this)) return this[index]
    }
}

console.log(nums.newFind(e => e%2===0 && e))

//find index:
Array.prototype.newFindIndex = function(callBack){

    for(let index = 0;index < this.length;index++){
        
        if(callBack(this[index], index, this)) return index
    }
    return -1
}

console.log(nums.newFindIndex(e => e%2===0 && e))

//every:
Array.prototype.newEvery = function(callBack){

    for(let index = 0;index < this.length;index++){
        
        if(!callBack(this[index], index, this)) return false
    }
    return true
}

console.log(nums.newEvery(e => e%2===0 && e))

//filter:
Array.prototype.newFilter = function(callBack){

    let newArray = []

    for(let index = 0;index < this.length;index++){

        if(callBack(this[index], index, this)) newArray.push(this[index])
    }
    return newArray
}

console.log(nums.newFilter(e => e%2===0 && e))


Array.prototype.newConcat = function(arrayToConcat){

    let newArray = []

    for(let index = 0;index < this.length;index++){
        newArray.push(this[index])
    }
    for(let index = 0;index < arrayToConcat.length;index++){
        newArray.push(arrayToConcat[index])
    }
    return newArray
}
let letters = ["a", "b", "c", "d"]
console.log(nums.newConcat(letters))

Array.prototype.newIndexOf = function(valueToSearch){
    for(index = 0;index < this.length;index++){
        if (this[index] === valueToSearch) return index
    }
    return -1    
}
console.log(nums.newIndexOf(11))

Array.prototype.newJoin = function(separator){
    let newString = this[0]

    for(index = 1;index < this.length;index++){
        newString += separator + this[index] 
    } 
    return newString
}
console.log(nums.newJoin(", "))

Array.prototype.newSlice = function(start, finish){
    let newArray = []

    if(finish === undefined){
        finish = this.length
    }

    for(let index = start;index < finish;index++){
        newArray.push(this[index])
    }

    return newArray
}
console.log(nums.newSlice(5))
